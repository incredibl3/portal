var express = require('express');
var router = express.Router();

router.use('/ace-builds', express.static(__dirname + '/../ace-builds'));

router.get('/', function(req, res) {
	res.render('editor', {text: "Tien dep trai"});
});

module.exports = router;