var express = require('express');
var router = express.Router();
var fs = require("fs");
var path = require('path');

var listApps = [];
var listMobileApps = [];
var isReady = false;
var isMobileReady = false;

router.use(function (req, res, next) {
  // console.log('Time:', Date.now());
  if (isReady && isMobileReady)
  	next();
  else {
  	var err = new Error('Not Found');
  	console.log("App is not ready yet!");
  	res.end();
  }
});

updateList();
updateMobileList();

function updateList() {
	console.log("updateList");

	// this portal backend is located at /home/devkit/
	// the apps are located at /var/www/play.incredibl3.cf/public_html/apps

	// fs.readdir("./apps",function(err, files){
	fs.readdir("../../var/www/play.incredibl3.cf/public_html/apps",function(err, files){
		if (err) {
			return console.error(err);
		}

		if (files.length == 0) {
			isReady = true;
			return;
		}

		var count = 0;
		files.forEach( function (file){
			count++;
			fs.stat(__dirname + '/../../../var/www/play.incredibl3.cf/public_html/apps/' + file, function (err, stats) {
				if (stats.isDirectory()) {
					var name =  __dirname + '/../../../var/www/play.incredibl3.cf/public_html/apps/' + file + "/package.json";

					fs.readFile(name, function (err, data) {
						if (err) {
							return console.error(err);
						}
						listApps[file] = JSON.parse(data.toString());
					});       			
				}
				if (count == files.length) {
					isReady = true;
				}			
			});
		});
	});
};

function getList() {
	if (isReady) {
		var result = [];
		for (var item in listApps) {
			result.push(listApps[item]);
		}
		return result;
	} else {
		
	}
};

function updateMobileList() {
	console.log("updateMobileList");

	// this portal backend is located at /home/devkit/
	// the apps are located at /var/www/m.play.incredibl3.cf/public_html/apps

	// fs.readdir("./apps",function(err, files){
	fs.readdir("../../var/www/m.play.incredibl3.cf/public_html/apps",function(err, files){
		if (err) {
			return console.error(err);
		}

		if (files.length == 0) {
			isMobileReady = true;
			return;
		}

		var count = 0;
		files.forEach( function (file){
			count++;
			fs.stat(__dirname + '/../../../var/www/m.play.incredibl3.cf/public_html/apps/' + file, function (err, stats) {
				if (stats.isDirectory()) {
					var name =  __dirname + '/../../../var/www/m.play.incredibl3.cf/public_html/apps/' + file + "/package.json";

					fs.readFile(name, function (err, data) {
						if (err) {
							return console.error(err);
						}
						listMobileApps[file] = JSON.parse(data.toString());
					});       			
				}
				if (count == files.length) {
					isMobileReady = true;
				}			
			});
		});
	});
};

function processReq(_p, res) {
	var resp = [];
	fs.readdir(_p, function(err, list) {
	  for (var i = list.length - 1; i >= 0; i--) {
	    resp.push(processNode(_p, list[i]));
	  }
	  res.json(resp);
	});
};

function processNode(_p, f) {
var s = fs.statSync(path.join(_p, f));
	return {
	  "id": path.join(_p, f),
	  "text": f,
	  "icon" : s.isDirectory() ? 'jstree-custom-folder' : 'jstree-custom-file',
	  "state": {
	    "opened": false,
	    "disabled": false,
	    "selected": false
	  },
	  "li_attr": {
	    "base": path.join(_p, f),
	    "isLeaf": !s.isDirectory()
	  },
	  "children": s.isDirectory()
	};
};

router.use(express.static(__dirname + '/../apps/'));

/* GET apps listing. */
router.get('/lists', function(req, res, next) {
  var result = [];
  for (var item in listApps) {
  	result.push(listApps[item]);
  }
  console.log(result);
  res.json(result);
  res.end();
});

/* GET apps listing. */
router.get('/mobilelists', function(req, res, next) {
  var result = [];
  for (var item in listMobileApps) {
  	result.push(listMobileApps[item]);
  }
  console.log(result);
  res.json(result);
  res.end();
});

/* GET total number of apps. */
router.get('/total', function(req, res) {
  res.send('respond with a total: ' + listApps.length);
});

/* GET the tree list. */
router.get('/tree', function(req, res) {
	// Follow this tutorial: http://thejackalofjavascript.com/file-browser-with-jstree-angularjs-and-expressjs/
	var _p;
	if (req.query.id == 1) {
	  _p = path.resolve(__dirname, '..', 'node_modules');
	  processReq(_p, res);

	} else {
	  if (req.query.id) {
	    _p = req.query.id;
	    processReq(_p, res);
	  } else {
	    res.json(['No valid data found']);
	  }
	}
});

router.get('/:key', function (req, res) {  
  var key = req.params.key;

  res.sendFile(path.join(__dirname + '/../apps/' + key + '/index.html'));
});

router.updateList = updateList;
router.getList = getList;
module.exports = router;