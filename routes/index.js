var express = require('express');
var router = express.Router();
var apps = require('./apps');

/* GET home page. */
router.get('/', function(req, res, next) {
	apps.updateList();
	res.sendFile(__dirname + "/../public/index.html");
});

module.exports = router;
