var express = require('express');
var router = express.Router();
var multer = require('multer');
var AdmZip = require('adm-zip');
var fs = require('fs');
var path = require('path');

var GameController = require('../controllers/GameController');
var gameController;

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error!'));
db.once('open', function() {
  console.log('Connect to database successfully!!!');
  gameController = new GameController(mongoose.connection);
});

var upload = multer({ dest: 'uploads/' });

router.use(function (req, res, next) {
  console.log('Time:', Date.now());
  next();
});

/* Show the upload form. */
router.get('/', function(req, res, next) {
	res.sendFile('upload.html', {root: './views'});
});

router.post('/', upload.single('fileupload'), function(req, res, next) {
  /** When using the "single"
      data come in "req.file" regardless of the attribute "name". **/
  var tmp_path = req.file.path;
  console.log("tmp_path: " + tmp_path);

  /** The original name of the uploaded file
      stored in the variable "originalname". **/
  var target_path = 'uploads/' + req.file.originalname;
  console.log("target_path: " + target_path);

  /** A better way to copy the uploaded file. **/
  var src = fs.createReadStream(tmp_path);
  var dest = fs.createWriteStream(target_path);
  src.pipe(dest);
  src.on('end', function() { fs.unlink(tmp_path); checkfile(target_path, req.file.originalname, res, next); });
  src.on('error', function(err) { fs.unlink(tmp_path); res.render('upload/error', {title: "Error"}); });
});

var checkfile = function(path, originalname, res, next) {
	try {
		var zip = new AdmZip(path);
		
		var packageJson;
		var folder_name = originalname.substr(0, originalname.indexOf("."));
		if (zip.readFile("package.json") != null) {
			packageJson = JSON.parse(zip.readAsText("package.json"))
		} else if (zip.readFile(folder_name + "/package.json") != null) {
			packageJson = JSON.parse(zip.readAsText(folder_name + "/package.json"));
		} else {
			// No package.json included
			return;
		}

		zip.extractAllTo("./apps/", true);
		
		fs.unlink(path);

		gameController.AddGameEntry({
			title: packageJson.title,
			status: 'In Review',
			icon: packageJson.icon,
			orientation: packageJson.orientation,
			location: packageJson.location
		}, next, function() {
			res.status(200).json({
				"files": [
					{
						"name": originalname,
						"size": 902604,
						"url": "http:\/\/example.org\/files\/picture1.jpg",
						"thumbnailUrl": "http:\/\/example.org\/files\/thumbnail\/picture1.jpg",
						"deleteUrl": "http:\/\/example.org\/files\/picture1.jpg",
						"deleteType": "DELETE"
					}
				]
			});
		});
	} catch (err) {
		// Error happens
		res.render('upload/error', {title: "Error"});
	}
};

module.exports = router;