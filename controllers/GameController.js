var GameModel = require('../models/GameModel');

/**
 * Abstract GameController constructor
 *
 * This is the controller of Game Model and View
 *
 * @param {Connection} conn A MongooseConnection instance
 * @param {Object} opts optional GameController options
 * @api public
 */

function GameController(conn, opts) {
  if (opts === void 0) {
    opts = {};
  }
  
  this._conn = conn;
}

/**
 * Add a game entry to the database
 *
 * @param {Object} info of the game to add
 * @api public
 */

GameController.prototype.AddGameEntry = function(gameInfo, next, cb) {
   var newGame = new GameModel({
       title: gameInfo.title,
       status: 'In Review',
       icon: gameInfo.icon,
       orientation: gameInfo.orientation,
       location: gameInfo.location,
       version: gameInfo.version
   });
   
   newGame.save(function(err) {
       if (err) {
           console.log('Game Entry Created Failed!');
           return next(err);
       }
        
       console.log('Game Entry Created successully!');
       cb(); 
   });
};

GameController.prototype.UpdateGameEntry = function() {
    
};

/**
 * Remove a game entry from the database
 *
 * @param {Object} info of the game to remove
 * @api public
 */

GameController.prototype.RemoveGameEntry = function(opts) {
  if (opts === void 0 || opts == {}) {
    console.log('Please specify condition to remove!');
    return;
  }
  
  GameModel.remove(opts, function(err){
     if (err)
        console.log('Can\'t remove: ' + JSON.stringify(opts));
     else  
        console.log('Successfully remove: ' + JSON.stringify(opts));
  });
};

/**
 * Find all the games that match the condition(s)
 *
 * @param {Object} condition(s) of the games 
 * @api public
 */
GameController.prototype.GetListOfGames = function(opts) {
  if (opts === void 0) {
    opts = {};
  }
  
  GameModel.find(opts, function(err, games) {
     if (err)
        console.log('Something wrong happened! Failed to get list of game with filter: ' + JSON.stringify(opts));
     else
        console.log('Retrieve list of game: ' + games);
  });
};

module.exports = GameController;