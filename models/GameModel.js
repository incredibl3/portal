var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var gameSchema = new Schema({
    title: {type: String, require: true, unique: true},
    status: String,
    icon: String,
    orientation: String,
    location: String,
    version: String
});

var GameModel = mongoose.model('game', gameSchema);

module.exports = GameModel;